var Checker = (function($) {
	var config = {
		apiUrl : "https://www.googleapis.com/pagespeedonline/v2/runPagespeed",
		// default set of links
		links : [
			"https://www.wikipedia.org/",
			"http://httparchive.org/",
			"http://www.w3schools.com/",
			"https://developer.mozilla.org/en-US/"
		],
		root : "", 		// jQuery wrapper around element containing visual components 
		siteList : "",	// jQuery wrapper around UL element containing list of the sites 
		resultsList : "",	// jQuery wrapper around an element containing resuls 
	};
	
	var root,
		siteList,
		addUrlForm,
		addUrlInput,
		resultsList,
		launchButton,
		resultsTemplate,
		links = Object.create(null);

	function init(customConfig) {
		// remove 'nojs' class in order to show full app version
		$("body").removeClass("nojs");
		
		// combine default settings with customer provided settings
		config = $.extend(config, customConfig);
		
		initUiVariables();
		
		// atach events only after all DOM elements are present
		attachEventHandlers();
	}

	function initUiVariables() {
		root = isMissedConfigValue("root") ? $('body') : config.root;
		addUrlForm = root.find("form#addurl");
		addUrlInput = addUrlForm.find("input[type=text]");
		launchButton = root.find(".launch");
		resultsTemplate = $("#resultsTemplate");
		

		siteList = createSiteList(config.links);
		if ( isMissedConfigValue("siteList") ) {
			root.append(siteList);
		} else {
			config.siteList.replaceWith(siteList);
		}
		if (siteList.length) {
			launchButton.prop('disabled', false);
		}
		
		resultsList = isMissedConfigValue("resultsList") ? $('body') : config.root;
		if ( isMissedConfigValue("resultsList") ) {
			resultsList = $("<div>", {"id" : "results"});
			root.append(resultsList);
		} else {
			resultsList = config.resultsList;
		}
	}
	
	function isMissedConfigValue(property) {
		return !config[property] || !config[property].length;
	}
	
	function createSiteList(sites) {
		var siteList = $("<ul>");

		if (!sites) return siteList;

		if (typeof sites === "string") {
			siteList.append( createSiteLink(site) );
		} else if (typeof sites === "object") {
			$.each(sites, function (index, site) {
				siteList.append( createSiteLink(site) );
			})
		}

		return siteList;
	}
	
	function createSiteLink (site) {
		links[site] = site;
		var result = $("<li>");
		var spinner = $("<img>", {"src" : "img/spinner.gif", "class" : "spinner"});
		var link = $("<a>", {"class" : "siteLink", "href" : site, "text" : site, "target" : "_blank"});
		var removeLink = $("<a>", {"class" : "removeLink", "href" : "#", "text" : "remove"});
		return result.append(spinner).append(link).append(removeLink);
	}
	
	function addSiteLink(site) {
		if (!links[site]) {
			var link = createSiteLink(site);
			siteList.append(link);
			addUrlInput.val("");
			return link;
		} else {
			addUrlForm.children(".errormessage")
				.text("site is already present")
				.show(200).delay(1500).hide(200);
		}
	}

	function removeSiteLink(site) {
		delete links[site];
		siteList
			.find('li').has('a[href="' + site + '"]')
			.remove();
	}
	function attachEventHandlers() {

		addUrlForm.on("submit", function (e) {
			e.preventDefault();
			addSiteLink( addUrlInput.val() );
			launchButton.prop('disabled', false);
		})

		siteList.on("click", "a.removeLink", function(e) {
			e.preventDefault();
			var element = $(e.target);
			var site = element.siblings("a.siteLink").attr("href");
			removeSiteLink(site);
			if (!Object.keys(links).length) {
				launchButton.prop('disabled', true);
			}
		})

		launchButton.on("click", function (e) {
			e.preventDefault();
			// clear previous results if they are already there
			var resultsSection = resultsList.parent();
			resultsSection.hide();
			resultsList.html("");
			// query result and show when they are ready
			$.each(links, function(index, link) {
				var liSite = siteList.find('li').has('a[href="' + link + '"]');
				checkSite(link, function (data) {
					liSite.removeClass("spinner");
					drawSiteResults(data).appendTo(resultsList);
					resultsSection.show();
				});
				liSite.addClass("spinner");
			});
			$(".slide-item")
		})

		resultsList.on("click", ".warnings a", function (e) {
			e.preventDefault();
			var $this = $(this);
			var parent = $this.parent();
			$this.siblings("ul").slideToggle("fast");
			if ( parent.hasClass("folded") ) {
				parent.removeClass("folded");
				var newText = "[-]";
			} else {
				parent.addClass("folded");
				var newText = "[+]";
			}
			$this.text(newText);
		})
	}
	
	function checkSite(link, success) {
		var strategy = "desktop";
		var queryString = $.param({"url" : link, "strategy" : strategy});
		var url = config.apiUrl + "?" + queryString;
		$.get(url, function(data) {
			data.strategy = strategy;
			success(data);
		})
	}

	function drawSiteResults(data) {
		
		var temp = resultsTemplate.html()
			.replace(/{{title}}/gi, data.title);
		temp = temp.replace(/{{strategy}}/gi, data.strategy);
		temp = temp.replace(/{{score}}/gi, data.ruleGroups.SPEED.score);
		var tempUl = $("<ul>");
		$.each(data.formattedResults.ruleResults, function (index, ruleResult) {
			if (ruleResult.ruleImpact && ruleResult.summary) {
				var text = ruleResult.summary.format;
				var arr = ruleResult.summary.args;
				text = fillTextParams (text, arr);
				var warning = $("<li>", {"text" : text});
				warning.appendTo(tempUl);
			}
		})
		
		var warnings = $("<div>").append(tempUl).html();
		temp = temp.replace(/{{warnings}}/gi, warnings);
		var $result;
		var optimizeImages = data.formattedResults.ruleResults.OptimizeImages;
		if (optimizeImages && +optimizeImages.ruleImpact > 0){
			var arrImages = data.formattedResults.ruleResults.OptimizeImages.urlBlocks[0].urls;
			var imgPreview = $("<div>").append(showImages(arrImages));
			temp = temp.replace(/{{imgPreview}}/gi, imgPreview.html());
			$result = $(temp);			$result.find("li.empty").removeClass("empty");
		} else {
			$result = $(temp);
		}
		return $result;
	}

    function fillTextParams (text, arr){
		for (var i = 0; arr && arr.length > i; i++){
			if(arr && arr[i].type == "HYPERLINK"){
				text = text.replace(/{{BEGIN_LINK}}/gi, '<a href="'+arr[i].value+'">');
				text = text.replace(/{{END_LINK}}/gi, '</a>');
			} else if (arr){
				var value = arr[i].value;
				var key = arr[i].key;
				var reg = new RegExp("{{"+key+"}}", "gi");
				text = text.replace(reg, value);
			}
		}
		return text;
	};
		
	function showImages (arrImages) {
		var slideWrap = $("<div>", {"class" : "carousel-items"});
		for (var i = 0; arrImages && arrImages.length > i; i++){
			var urlValue = arrImages[i].result.args[0].value;
			var slideItem = $("<div class='carousel-block'>")
				.append($( "<img>", {"src" : urlValue, "alt" : ""}));
			slideItem.appendTo(slideWrap);
		}
		return slideWrap;
	};
	
	return {
		"init" : init,
		"checkSite" : checkSite
	}
})(jQuery);